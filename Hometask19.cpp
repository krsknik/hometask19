﻿#include <iostream>
#include <vector>

class Animal
{

public:
    virtual void Voice() = 0;
    virtual ~Animal() {}
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Caw : public Animal
{
    void Voice() override
    {
        std::cout << "Moooo!";
    }
};

int main()
{
    Animal* array[3] = { new Cat, new Dog, new Caw };
 
    for (size_t i = 0; i < 3; ++i) 
    {
        array[i]->Voice();
    }
    
    for (size_t i = 0; i < 3; ++i) 
    {
        delete array[i];
    }
    return 0;
};
